from turtle import *

# Kodus täiendada, lisada 1 tase või lahendada kogu ülesanne rekursiivselt. Soovitan rekursiooni
def f(length, depth):
   if depth == 0:
     forward(length)
   else:
     forward(length)
     left(60)
     forward(length)
     right(120)
     forward(length)
     left(60)
     forward(length)
 
 
f(100, 4)
 

def drawSpiral(lineLen):
    if lineLen > 0: # peatumistingimus
        forward(lineLen)
        right(90)
        drawSpiral(lineLen-5)

##drawSpiral(300)

def tree(branchLen):
    if branchLen > 5:
        forward(branchLen)
        right(20)
        tree(branchLen-15)
        left(40)
        tree(branchLen-15)
        right(20)
        backward(branchLen)


##left(90)
##tree(75)

def puu(kylg,tase):
    if tase >= 0:    
        forward(kylg) # igal juhul teeme seda
        right(30)
        # parem haru, siia tekib kordus
        pencolor('blue')
        puu(kylg*0.7,tase-1)
        # liigume vasakule
        left(60)
        # liigume vasakusse harusse
        pencolor('green')
        puu(kylg*0.7,tase-1)
        
        # liigume tagasi
        right(30)
        forward(-kylg)
    else:
        return
##left(90)
##puu(100,3)









def drawTree(level,size):
    
    if level >= 0:
            angle = 30
            forward(size)
            #Parem haru
            right(30)
            drawTree(level-1,size*0.7)
            left(2*30)
            #Vasak haru
            drawTree(level-1,size*0.7)
            right(30)
            forward(-size)
    else:
       #Stop the recursion
        return

##left(90)
#exitonclick()

def ruut(x):
    forward(x)
    left(90)
    forward(x)
    left(90)
    forward(x)
    left(90)
    forward(x)
    left(90)

def ringRuut(x,deg):
##    turn = deg
    ##if turn < 361:
    ruut(x)
    left(deg)
        ##turn += deg
    ringRuut(x, deg)
    

