## ruudu kontrollimiseks, igast reast 3 elementi
rida1 = [1,2,3]
rida2 = [4,5,6]
rida3 = [7,8,9]

def ruut(rida1,rida2,rida3):    
    # iga ruut annab kindla summa 1+2+3+4+5+6+7+8+9 = 45
    ruut = rida1 + rida2 + rida3

    summa = 0
    for i in ruut:
        summa = summa + i
        
    if summa == 45:
        print('õige ruut')
    else:
        print('midagi on valesti')

# rea kontrolliks
rida = [1,2,3,4,5,6,7,8,9]
def reaK(rida):
    summa = 0
    for i in rida:
        summa += i
        
    if summa == 45:
        print('õige rida')
    else:
        print('midagi on reas valesti')
    
    
r1 = [1]
r2= [2]
r3 = [3]
r4 = [4]
r5 = [5]
r6 = [6]
r7 = [7]
r8 = [8]
r9 = [9]
def tulp(r1,r2,r3,r4,r5,r6,r7,r8,r9):
    tulp = r1 + r2 + r3 +r4 +r5 +r6 +r7+r8+r9
    summa = 0
    for i in tulp:
        summa += i
        
    if summa == 45:
        print('õige tulp')
    else:
        print('midagi on tulbas valesti')
    
    
    